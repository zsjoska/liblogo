FROM python:3.9.0-slim

ENV APP_VERSION="5.0.1" \
    APP="platformio-core"

LABEL app.name="${APP}" \
      app.version="${APP_VERSION}" \
      maintainer="Sebastian Glahn <hi@sgla.hn>"

RUN apt update && apt install -y g++

RUN pip install -U platformio==${APP_VERSION} && \
    mkdir -p /workspace && \
    mkdir -p /.platformio && \
    chmod a+rwx /.platformio

RUN pio platform install native
# USER 1001

WORKDIR /workspace
CMD ["platformio", "test"]

# docker build -t zsjoska/platformio-core-native .
# docker run --rm -v "$(pwd)":/workspace zsjoska/platformio-core-native
# docker push zsjoska/platformio-core-native:latest
# docker login
