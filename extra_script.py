Import("env")

#
# Dump build environment (for debug)
# print(env.Dump())
#

fsanitize = list(filter(lambda x: "fsanitize" in x, env["CCFLAGS"]))

env.Append(
    LINKFLAGS=fsanitize
)

# bind running native tests to upload button
AlwaysBuild(env.Alias("upload", "$BUILD_DIR/${PROGNAME}", "./.pio/build/native/program"))
