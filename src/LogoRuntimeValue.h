#pragma once

#include <vector>

#include "utils.h"
#include "Expression.h"
#include "Node.h"

class Function {
public:
    const Identifier *name;
    const std::vector<const Variable *> parameters;
    const BlockStatement *body;

    Function(const Identifier *name, const std::vector<const Variable *> &parameters, const BlockStatement *body)
            : name(name->copy()),
              parameters(Variable::copyVariables(parameters)),
              body(body->copy()) {}

    ~Function() {
        delete name;
        for(const auto &param:parameters){
            delete param;
        }
        delete body;
    }
};

enum LogoRuntimeValueType {
    kIntRuntimeType,
    kBoolRuntimeType,
    kStrRuntimeType,
    kDoubleRuntimeType,
    kFuncRuntimeType,
};

class LogoRuntimeValue {
    LogoRuntimeValueType type;
    void *ptr;
protected:
    LogoRuntimeValue(LogoRuntimeValueType type, void *ptr) : type{type}, ptr{ptr} {}

public:

    virtual ~LogoRuntimeValue();

    int getInt();

    double getDouble();

    bool getBool();

    char *getStr();

    Function *getFunction();

    bool isBool() {
        return type == kBoolRuntimeType;
    }

    bool isDouble() {
        return type == kDoubleRuntimeType;
    }

    bool isInt() {
        return type == kIntRuntimeType;
    }

    bool isStr() {
        return type == kStrRuntimeType;
    }

    bool isFunction() {
        return type == kFuncRuntimeType;
    }

    explicit LogoRuntimeValue(LogoRuntimeValue *pValue);
};

class IntValue : public LogoRuntimeValue {
public:
    explicit IntValue(int value) : LogoRuntimeValue(kIntRuntimeType, new int{value}) {}
};

class BoolValue : public LogoRuntimeValue {
public:
    explicit BoolValue(bool value) : LogoRuntimeValue(kBoolRuntimeType, new bool{value}) {}
};

class DoubleValue : public LogoRuntimeValue {
public:
    explicit DoubleValue(double value) : LogoRuntimeValue(kDoubleRuntimeType, new double{value}) {}
};

class StrValue : public LogoRuntimeValue {
public:
    explicit StrValue(const char *value) : LogoRuntimeValue(kStrRuntimeType, copyStr(value)) {}
};

class FuncValue : public LogoRuntimeValue {
public:
    explicit FuncValue(Function *function) : LogoRuntimeValue(kFuncRuntimeType, function) {}
};

typedef LogoRuntimeValue *(*BuiltInFuncPtr)(std::vector<LogoRuntimeValue *> &);
