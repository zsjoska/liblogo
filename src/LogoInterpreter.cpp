#include "LogoInterpreter.h"
#include "Lexer.h"
#include "Parser.h"

bool LogoInterpreter::execute(const char *code) {

    Lexer lexer(code);
    Parser parser(lexer);
    auto program = parser.parse();

    auto result = runtime->run(program);
    delete result;
    delete program;
    return !runtime->hasErrors();
}

Program *LogoInterpreter::parse(const char *code) {
    Lexer lexer(code);
    Parser parser(lexer);
    return parser.parse();
}