#include <iostream>

#include "LogoRuntime.h"
#include "Lexer.h"
#include "Parser.h"
#include "utils.h"

LogoRuntime::~LogoRuntime() {
    for (const auto &function:functions) {
        delete[] function.first;
        delete function.second;
    }
    functions.clear();
}

LogoRuntimeValue *LogoRuntime::run(Program *program) {
    clearErrors();
    if (program->hasErrors()) {
        importErrors(program->getParseErrors());
        return nullptr;
    }
    return eval(program->getStatements());
}

LogoRuntimeValue *LogoRuntime::eval(const Node *node) {
    switch (node->logoNode) {
        case kEmptyNodeLogoNode:
            return nullptr;
        case kBlockStatementLogoNode:
            return eval(((const BlockStatement *) node)->getStatements());
        case kIdentifierLogoNode:
            return eval((const Identifier *) node);
        case kVariableLogoNode:
            return eval((const Variable *) node);
        case kExprStatementLogoNode:
            return eval(((const ExpressionStatement *) node)->getExpression());
        case kBooleanLiteralLogoNode:
            return new BoolValue{((const BooleanLiteral *) node)->value};
        case kNumberLiteralLogoNode:
            return new DoubleValue{((const NumberLiteral *) node)->value};
        case kFuncLiteralLogoNode: {
            auto funcLiteral = (const FunctionLiteral *) node;
            auto function = new Function(funcLiteral->getName(), funcLiteral->getArguments(), funcLiteral->getBody());
            functions[copyStr(funcLiteral->getName()->getLiteral())] = function;
            return new FuncValue(function);
        }
        case kIfExprLogoNode:
            return eval((IfExpression *) node);
        case kRepeatExprLogoNode:
            return eval((RepeatExpression *) node);
        case kPrefixExprLogoNode: {
            auto expr = (PrefixExpression *) node;
            auto right = eval(expr->getRight());
            return evalPrefixExpression(expr->sourceMark, expr->getOp(), right);
        }
        case kInfixExprLogoNode: {
            auto expr = (InfixExpression *) node;
            auto left = eval(expr->getLeft());
            auto right = eval(expr->getRight());
            return evalInfixExpression(expr->sourceMark, expr->getOp(), left, right);
        }
        case kCallExprLogoNode:
            return eval((CallExpression *) node);
        default:
            registerError(node->sourceMark, "Unknown statement");
            break;
    }
    return nullptr;
}

LogoRuntimeValue *LogoRuntime::eval(const std::vector<const Statement *> &statements) {
    LogoRuntimeValue *result = nullptr;
    for (const auto &statement:statements) {
        delete result;

        result = eval(statement);

        if (result == nullptr) {
            return result;
        }
    }
    return result;
}

LogoRuntimeValue *LogoRuntime::eval(const Identifier *identifier) {
//    Globals not implemented yet
//    auto global = globals[identifier->getLiteral()];
//    if (global != nullptr) {
//        return nullptr;
//    }

    auto builtinFunc = builtinFunctions.find(identifier->getLiteral());
    if (builtinFunc != builtinFunctions.end()) {
        return new StrValue{identifier->getLiteral()};
    }

    auto pos = functions.find(identifier->getLiteral());
    if (pos != functions.end()) {
        return new FuncValue(pos->second);
    }

    registerError(identifier->sourceMark, "Identifier '%s' not found.", identifier->getLiteral());
    return nullptr;
}

LogoRuntimeValue *LogoRuntime::eval(const Variable *variable) {
    auto var = runtimeStack.getValue(variable->getLiteral());
    if (var != nullptr) {
        return var;
    }

    registerError(variable->sourceMark, "Variable %s not found", variable->getLiteral());
    return nullptr;
}

std::vector<LogoRuntimeValue *> LogoRuntime::eval(
        const std::vector<const Expression *> &expressions) {
    std::vector<LogoRuntimeValue *> result;
    LogoRuntimeValue * evaluated = nullptr;
    for (const auto &expression:expressions) {
        evaluated = eval(expression);
        result.push_back(evaluated);
        if (evaluated == nullptr) break;
    }

    if (evaluated == nullptr && !result.empty()){
        // Error situation, we have to leave 1 null result, we know that he last one is such
        // so we have to delete the front
        while (result.size() > 1){
            delete result[0];
            result.erase(result.begin());
        }
    }

    return result;
}

LogoRuntimeValue *LogoRuntime::eval(const IfExpression *ifExpression) {
    LogoRuntimeValue *result = nullptr;
    auto condition = eval(ifExpression->getCondition());
    if (condition != nullptr && condition->isBool()) {
        if (condition->getBool()) {
            result = eval(ifExpression->getConsequence());
        } else if (ifExpression->getAlternative() != nullptr) {
            result = eval(ifExpression->getAlternative());
        } else {
            result = new BoolValue{false};
        }
    } else {
        registerError(ifExpression->sourceMark, "if expression must evaluate to boolean");
    }

    delete condition;
    return result;
}

LogoRuntimeValue *LogoRuntime::eval(const RepeatExpression *repeat) {
    LogoRuntimeValue *result = nullptr;
    auto repeatCount = repeat->getCount()->value;
    for (int i = 0; i < repeatCount; ++i) {
        delete result;
        result = eval(repeat->getStatement());

        if (result == nullptr) {
            break;
        }
    }
    return result;
}

LogoRuntimeValue *LogoRuntime::evalPrefixExpression(const SourceMark &sourceMark, const char *op,
                                                    LogoRuntimeValue *right) {
    LogoRuntimeValue *result = nullptr;
    switch (op[0]) {
        case '-':
            result = evalMinusPrefixOperatorExpression(sourceMark, right);
            break;
        default:
            registerError(sourceMark, "Unknown operator %s", op);
    }
    delete right;
    return result;
}

LogoRuntimeValue *
LogoRuntime::evalMinusPrefixOperatorExpression(const SourceMark &sourceMark, LogoRuntimeValue *right) {
    if (right != nullptr && right->isDouble()) {
        return new DoubleValue{-right->getDouble()};
    } else if (right != nullptr && right->isInt()) {
        return new IntValue{-right->getInt()};
    } else if (right != nullptr) {
        registerError(sourceMark, "Invalid type");
    }
    return nullptr;
}

LogoRuntimeValue *LogoRuntime::evalInfixExpression(const SourceMark &sourceMark, const char *op,
                                                   LogoRuntimeValue *left, LogoRuntimeValue *right) {
    LogoRuntimeValue *result = nullptr;
    if (left->isDouble() && right->isDouble()) {
        result = evalDoubleInfixExpression(sourceMark, op, left->getDouble(), right->getDouble());
    } else if (left->isDouble() && right->isInt()) {
        result = evalDoubleInfixExpression(sourceMark, op, left->getDouble(), right->getInt());
    } else if (left->isInt() && right->isDouble()) {
        result = evalDoubleInfixExpression(sourceMark, op, left->getInt(), right->getDouble());
        /*}  else if (left->getInt() != nullptr && right->getInt() != nullptr) {
        } else if (left->getBool() != nullptr && right->getBool() != nullptr) {
        } else if (left->getStr() != nullptr && right->getStr() != nullptr) {

        } */
    } else {
        registerError(sourceMark, "Unable to evaluate expression %s", op);
    }

    delete left;
    delete right;
    return result;
}

LogoRuntimeValue *
LogoRuntime::evalDoubleInfixExpression(const SourceMark &sourceMark, const char *op, double left, double right) {
    switch (op[0]) {
        case '+':
            return new DoubleValue{left + right};
        case '-':
            return new DoubleValue{left - right};
        case '*':
            return new DoubleValue{left * right};
        case '/': {
            if (right == 0.0) {
                registerError(sourceMark, "Division by 0");
                return nullptr;
            }
            return new DoubleValue{left / right};
        }
        case '<':
            return new BoolValue{left < right};
        case '>':
            return new BoolValue{left > right};
        case '=':
            return new BoolValue{left == right};
            // TODO:  case '!=' and others
    }
    registerError(sourceMark, "Unknown operator %s for number", op);
    return nullptr;
}

LogoRuntimeValue *LogoRuntime::eval(const CallExpression *call) {
    LogoRuntimeValue *result = nullptr;
    auto expression = eval(call->getName());
    if (expression != nullptr) {
        if (expression->isStr()) {
            auto funcName = expression->getStr();
            auto args = eval(call->getArguments());
            if (args.size() == 1 && args[0] == nullptr) {
                // Argument evaluation error condition
                delete expression;
                return nullptr;
            }

            result = executeBuiltinFunction(funcName, args);

            for (const auto &arg:args) {
                delete arg;
            }
            args.clear();
        } else if (expression->isFunction()) {
            auto args = eval(call->getArguments());
            if (args.size() == 1 && args[0] == nullptr) {
                // Argument evaluation error condition
                delete expression;
                return nullptr;
            }

            result = executeDefinedFunction(call->sourceMark, expression->getFunction(), args);

            for (const auto &arg:args) {
                delete arg;
            }
            args.clear();
        } else {
            registerError(call->sourceMark, "Invalid function value");
        }
        delete expression;
    }
    return result;
}

LogoRuntimeValue *LogoRuntime::executeBuiltinFunction(const char *funcName,
                                                      std::vector<LogoRuntimeValue *> &args) {
    auto func = builtinFunctions.find(funcName);
    if (func != builtinFunctions.end()) {
        return func->second(args);
    }
    return nullptr;
}

LogoRuntimeValue *LogoRuntime::executeDefinedFunction(const SourceMark &sourceMark, Function *function,
                                                      std::vector<LogoRuntimeValue *> &arguments) {
    if (function->parameters.size() != arguments.size()) {
        registerError(sourceMark, "Expecting %d actual parameter(s)", function->parameters.size());
        return nullptr;
    }

    runtimeStack.push();
    for (unsigned int i = 0; i < arguments.size(); ++i) {
        runtimeStack.setStackValue(function->parameters[i]->getLiteral(), arguments[i]);
    }
    auto result = eval(function->body);
    runtimeStack.pop();
    return result;
}
