#pragma once

#include <vector>
#include <iomanip>
#include <ostream>
#include <iostream>

#include "utils.h"
#include "Token.h"

class Lexer {
    const char *code;
    unsigned int currentPosition;
    char currentChar;
    unsigned int source_line = 1;
    unsigned int source_column = 0;
    std::vector<Token *> tokens;

    Token *parseNextToken();

public:
    explicit Lexer(const char *code);

    virtual ~Lexer();

    void clearTokens();

    std::vector<Token *> lex();

    Token *nextToken();

    void skipWhitespace();

    void readChar();

    char nextChar();

    char *readString();

    char *readNumber();

    static TokenType mapKeyword(const char *token);
};
