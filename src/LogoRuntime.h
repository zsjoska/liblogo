#pragma once

#include <map>
#include <cstring>
#include <vector>

#include "Node.h"
#include "Expression.h"
#include "WithErrors.h"
#include "utils.h"
#include "LogoRuntimeValue.h"
#include "Program.h"

struct cmp_str {
    bool operator()(char const *a, char const *b) const {
        return std::strcmp(a, b) < 0;
    }
};

class GlobalVariable {
    const char *variableName;
    char *value;
public:
    virtual void set(const char *value) = 0;

    virtual void get() = 0;

    const char *name() {
        return variableName;
    }

    explicit GlobalVariable(const char *name) :
            variableName(name), value(nullptr) {
    }

    virtual ~GlobalVariable() {
        if (value != nullptr) {
            delete[] value;
            value = nullptr;
        }
    }
};

class Stack {
    std::vector<std::map<const char *, LogoRuntimeValue *, cmp_str>> stack;
public:
    Stack() = default;

    //TODO: here we may need to cleanup the stack
    ~Stack() = default;

    bool push() {
        std::map<const char *, LogoRuntimeValue *, cmp_str> layer;
        stack.push_back(layer);
        return true;
    }

    bool pop() {
        if (stack.empty()) {
            // error
            return false;
        }
        auto layer = stack.back();

        for (const auto &var:layer) {
            delete[] var.first;
            delete var.second;
        }
        layer.clear();

        stack.pop_back();
        return true;
    }

    bool setStackValue(const char *key, LogoRuntimeValue *value) {
        if (stack.empty()) {
            return false;
        }

        auto layer = stack.back();
        auto oldValuePos = layer.find(key);
        auto oldValue = oldValuePos != layer.end() ? oldValuePos->second : nullptr;
        delete oldValue;
        layer[copyStr(key)] = new LogoRuntimeValue(value);
        // TODO: clarify this mess with back&pop&push
        stack.pop_back();
        stack.push_back(layer);
        return true;
    }

    LogoRuntimeValue *getValue(const char *key) {
        if (stack.empty()) {
            return nullptr;
        }
        auto layer = stack.back();

        auto oldValuePos = layer.find(key);
        return oldValuePos != layer.end() ? new LogoRuntimeValue(oldValuePos->second) : nullptr;
    }
};

class LogoRuntime : public WithErrors {
public:
    LogoRuntime()= default;

    ~LogoRuntime() override;

    LogoRuntimeValue *run(Program *program);

    void registerVariable(GlobalVariable *g) {
        globals[g->name()] = g;
    }

    void registerBuiltInFunc(const char *name, BuiltInFuncPtr ptr) {
        builtinFunctions[name] = ptr;
    }

private:
    std::map<const char *, BuiltInFuncPtr, cmp_str> builtinFunctions;
    std::map<const char *, GlobalVariable *, cmp_str> globals;
    std::map<const char *, Function *, cmp_str> functions;
    Stack runtimeStack;

    LogoRuntimeValue *eval(const Node *node);

    LogoRuntimeValue *eval(const std::vector<const Statement *> &statements);

    LogoRuntimeValue *eval(const Identifier *identifier);

    LogoRuntimeValue *eval(const Variable *variable);

    std::vector<LogoRuntimeValue *> eval(const std::vector<const Expression *> &expressions);

    LogoRuntimeValue *eval(const IfExpression *variable);

    LogoRuntimeValue *eval(const RepeatExpression *variable);

    LogoRuntimeValue *eval(const CallExpression *expression);

//    LogoRuntimeValue *evalNumberLiteral(NumberLiteral *num);

    LogoRuntimeValue *evalPrefixExpression(const SourceMark &sourceMark, const char *op, LogoRuntimeValue *right);

    LogoRuntimeValue *evalMinusPrefixOperatorExpression(const SourceMark &sourceMark, LogoRuntimeValue *right);

    LogoRuntimeValue *evalInfixExpression(const SourceMark &sourceMark, const char *op, LogoRuntimeValue *left,
                                          LogoRuntimeValue *right);

    LogoRuntimeValue *
    evalDoubleInfixExpression(const SourceMark &sourceMark, const char *op, double left, double right);


    LogoRuntimeValue *executeBuiltinFunction(const char *funcName,
                                             std::vector<LogoRuntimeValue *> &args);

    LogoRuntimeValue *executeDefinedFunction(const SourceMark &sourceMark, Function *function,
                                             std::vector<LogoRuntimeValue *> &arguments);
};
