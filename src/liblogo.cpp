#ifndef UNIT_TEST

#include <iostream>
#include "Lexer.h"
#include "Parser.h"
#include "LogoInterpreter.h"
#include "LogoRuntime.h"

#include "stdio.h"

const char *tree = "to tree :size\n"
                   "  if (:size < 5) [\n"
                   "    fd(:size) bk(:size)\n"
                   "  ] else [\n"
                   "    fd(:size/3)  \n"
                   "    lt(30) tree(:size*2/3) rt(30)\n"
                   "    fd(:size/6)\n"
                   "    rt(25) tree(:size/2) lt(25)\n"
                   "    fd(:size/3)\n"
                   "    rt(25) tree(:size/2) lt(25)\n"
                   "    fd(:size/6)\n"
                   "    bk(:size)\n"
                   "  ]\n"
                   "end\n"
                   "\n"
                   "tree(350)";

const char *square = "repeat 4 [fd(100) rt(90)]";

const char *fd = "fd(50)";
const char *fd_bk = "fd(50) bk(20)";

int main1() {
    Lexer lexer("fd(50)\nfd(50)");
    auto tokens = lexer.lex();
    for (const auto &token:tokens) {
        token->print(std::cout);
    }
    return 0;
}

int main2() {
    Lexer lexer("to func :param\nfd(50)\nend");
    Parser parser(lexer);
    auto program = parser.parse();
    if (parser.hasErrors()) {
        auto errors = parser.getParseErrors();
        for (const auto &error:errors) {
            std::cout << error << std::endl;
        }
    }
    program->print(std::cout);
    return 0;
}

LogoRuntimeValue *logo_fd(std::vector<LogoRuntimeValue *> &params) {
    auto param1 = params[0]->getDouble();
    printf("Going forward %f\n", param1);
    return new IntValue{216};
}

LogoRuntimeValue *logo_bk(std::vector<LogoRuntimeValue *> &params) {
    auto param1 = params[0]->getDouble();
    printf("Going backward %f\n", param1);
    return new IntValue{215};
}

LogoRuntimeValue *logo_lt(std::vector<LogoRuntimeValue *> &params) {
    auto param1 = params[0]->getDouble();
    printf("Turn Left %f\n", param1);
    return new IntValue{214};
}
LogoRuntimeValue *logo_rt(std::vector<LogoRuntimeValue *> &params) {
    auto param1 = params[0]->getDouble();
    printf("Turn Right %f\n", param1);
    return new IntValue{213};
}

LogoRuntimeValue *logo_bool(std::vector<LogoRuntimeValue *> &params) {
    auto param1 = params[0]->getBool();
    printf("Bool %d\n", param1);
    return new IntValue{213};
}

int main3() {
    auto code = "to tree :size\n"
                "  if (:size < 5) [\n"
                "    fd(:size) bk(:size)\n"
                "  ] else [\n"
                "    fd(:size/3)  \n"
                "    lt(30) tree(:size*2/3) rt(30)\n"
                "    fd(:size/6)\n"
                "    rt(25) tree(:size/2) lt(25)\n"
                "    fd(:size/3)\n"
                "    rt(25) tree(:size/2) lt(25)\n"
                "    fd(:size/6)\n"
                "    bk(:size)\n"
                "  ]\n"
                "end\n"
                "\n"
                "tree(350)";
    LogoInterpreter interpreter;
    interpreter.registerBuiltInFunc("fd", &logo_fd);
    interpreter.registerBuiltInFunc("bk", &logo_bk);
    interpreter.registerBuiltInFunc("rt", &logo_rt);
    interpreter.registerBuiltInFunc("lt", &logo_lt);
    interpreter.registerBuiltInFunc("bool", &logo_bool);
    std::cout << code << std::endl;
    interpreter.execute(code);
    std::cout << interpreter.asOneError() << std::endl;
    return 0;
}

int main4() {
    auto code = "fd(50) bk(20)";
    LogoInterpreter interpreter;
    interpreter.registerBuiltInFunc("fd", &logo_fd);
    interpreter.registerBuiltInFunc("bk", &logo_bk);
    interpreter.registerBuiltInFunc("rt", &logo_rt);
    interpreter.registerBuiltInFunc("lt", &logo_lt);
    interpreter.registerBuiltInFunc("bool", &logo_bool);
    std::cout << code << std::endl;
    auto program = interpreter.parse(code);
    auto runtime = interpreter.getRuntime();
    runtime->run(program);
    runtime->run(program);
    std::cout << interpreter.asOneError() << std::endl;
    return 0;
}

int main() {
//	main1();
//    main2();
    main3();
//    main4();
    return 0;
}

#endif
