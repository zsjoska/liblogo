#include "Token.h"

Token::Token(TokenType type, SourceMark sourceMark, const char *literal) :
        type(type), sourceMark(sourceMark), literal(literal) {
}

Token::Token(TokenType type, SourceMark sourceMark, char c) :
        type(type), sourceMark(sourceMark), literal(new char[2]{c, '\0'}) {
}

Token::~Token() {
    delete[] literal;
}
