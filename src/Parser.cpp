#include "Parser.h"

#include <vector>
#include <cstdlib>
#include <cerrno>

Parser::Parser(Lexer &lexer) :
        lexer(lexer) {
    currentToken = lexer.nextToken();
    nextToken = lexer.nextToken();
}

Precedence Parser::mapPrecedence(TokenType type) {
    switch (type) {
        case kEqualsToken:
        case kNotEqualsToken:
            return kEqualsPrecedence;
        case kLTToken:
        case kGTToken:
            return kLessGreaterPrecedence;
        case kPlusToken:
        case kMinusToken:
            return kSumPrecedence;
        case kSlashToken:
        case kAsterixToken:
            return kProductPrecedence;
        case kLParenToken:
            return kCallPrecedence;
        default:
            return kLowestPrecedence;
    }
}

Program *Parser::parse() {
    std::vector<const Statement *> statements;

    while (currentToken->type != kEofToken) {
        statements.push_back(parseStatement());
        advance();
    }
    auto program = new Program(statements);
    program->importErrors(getParseErrors());
    return program;
}

void Parser::advance() {
    currentToken = nextToken;
    nextToken = lexer.nextToken();
}

Precedence Parser::nextPrecedence() {
    return mapPrecedence(nextToken->type);
}

Precedence Parser::currentPrecedence() {
    return mapPrecedence(currentToken->type);
}

bool Parser::expectNext(TokenType type) {
    if (nextToken->type == type) {
        advance();
        return true;
    } else {
        registerError(nextToken->sourceMark, "Unexpected token '%s'", nextToken->literal);
        return false;
    }
}

Statement *Parser::parseStatement() {
    return new ExpressionStatement(currentToken, parseExpression());
}

Identifier *Parser::parseIdentifier() {
    return new Identifier(currentToken, currentToken->literal);
}

Expression *Parser::parseBoolean() {
    return new BooleanLiteral(currentToken, currentToken->type == kTrueToken);
}

Variable *Parser::parseVariable() {
    return new Variable(currentToken, currentToken->literal);
}

NumberLiteral *Parser::parseNumber() {

    char *e;
    errno = 0;
    double value = std::strtod(currentToken->literal, &e);
    if (*e != '\0' || errno != 0) {
        registerError(currentToken->sourceMark, "could not parse %s as Number", nextToken->literal);
    }

    return new NumberLiteral(currentToken, value);
}

Expression *Parser::parseIfExpression() {
    auto pToken = currentToken;
    if (!expectNext(kLParenToken)) {
        return new EmptyNode();
    }

    advance();
    auto condition = parseExpression();

    if (!expectNext(kRParenToken)) {
        delete condition;
        return new EmptyNode();
    }

    if (!expectNext(kLBracketToken)) {
        delete condition;
        return new EmptyNode();
    }
    advance();

    auto consequence = parseBlockStatement();
    BlockStatement *alternative = nullptr;
    if (nextToken->type == kElseToken) {
        advance();
        if (!expectNext(kLBracketToken)) {
            delete condition;
            delete consequence;
            return new EmptyNode();
        }
        advance();
        alternative = parseBlockStatement();
    }

    return new IfExpression(pToken, condition, consequence, alternative);
}

BlockStatement *Parser::parseBlockStatement() {
    auto pToken = currentToken;
    std::vector<const Statement *> statements;
    while (currentToken->type != kRBracketToken
           && currentToken->type != kEofToken) {
        statements.push_back(parseStatement());
        advance();
    }
    if (currentToken->type != kRBracketToken) {
        for (const auto &statement:statements) {
            delete statement;
        }
        statements.clear();
        registerError(pToken->sourceMark, "Block statement not closed");
    }
    return new BlockStatement(pToken, statements);
}

Expression *Parser::parseFunctionLiteral() {
    auto pToken = currentToken;
    advance();
    auto name = parseIdentifier();
    advance();

    std::vector<const Variable *> parameters;
    while (currentToken->type == kVariableToken) {
        parameters.push_back(parseVariable());
        advance();
    }

    std::vector<const Statement *> statements;
    while (currentToken->type != kEndToken && currentToken->type != kEofToken) {
        statements.push_back(parseStatement());
        advance();
    }

    if (currentToken->type == kEofToken) {
        // TODO: don't create FunctionLiteral
        registerError(pToken->sourceMark, "Missing end for procedure");
    }
    auto body = new BlockStatement(pToken, statements);

    return new FunctionLiteral(pToken, name, parameters, body);
}

Expression *Parser::parseRepeatExpression() {
    auto pToken = currentToken;
    if (!expectNext(kNumberToken)) {
        return new EmptyNode();
    }

    auto count = parseNumber();

    if (!expectNext(kLBracketToken)) {
        delete count;
        return new EmptyNode();
    }

    advance();

    auto statement = parseBlockStatement();

    return new RepeatExpression(pToken, count, statement);
}

Expression *Parser::parsePrefixExpression() {
    auto pToken = currentToken;
    auto op = pToken->literal;
    advance();
    return new PrefixExpression(pToken, op, parseExpression(kPrefixPrecedence));
}

Expression *Parser::parseGroupedExpression() {
    advance();
    auto expression = parseExpression();

    if (!expectNext(kRParenToken)) {
        delete expression;
        return new EmptyNode();
    }
    return expression;
}

Expression *Parser::callPrefixFunction(TokenType type) {
    switch (type) {
        case kIdentifierToken:
            return parseIdentifier();
        case kVariableToken:
            return parseVariable();
        case kIfToken:
            return parseIfExpression();
        case kToToken:
            return parseFunctionLiteral();
        case kRepeatToken:
            return parseRepeatExpression();
        case kTrueToken:
        case kFalseToken:
            return parseBoolean();
        case kNumberToken:
            return parseNumber();
        case kMinusToken:
            return parsePrefixExpression();
        case kLParenToken:
            return parseGroupedExpression();
        default:
            registerError(currentToken->sourceMark, "no prefix parse function for %s", currentToken->literal);
            return new EmptyNode();
    }
}

InfixParseFuncPtr Parser::mapInfixFunction(TokenType type) {
    switch (type) {
        case kPlusToken:
        case kMinusToken:
        case kSlashToken:
        case kAsterixToken:
        case kEqualsToken:
        case kNotEqualsToken:
        case kLTToken:
        case kGTToken:
            return &Parser::parseInfixExpression;
        case kLParenToken:
            return &Parser::parseCallExpression;
        default:
            return nullptr;
    }
}

Expression *Parser::parseExpression(Precedence precedence) {
    auto leftExp = callPrefixFunction(currentToken->type);
    while (precedence < nextPrecedence()) {
        auto infix = mapInfixFunction(nextToken->type);
        if (infix == nullptr) {
            return leftExp;
        }
        advance();
        leftExp = (this->*infix)(leftExp);
    }
    return leftExp;
}

Expression *Parser::parseInfixExpression(Expression *left) {
    auto pToken = currentToken;
    auto op = pToken->literal;
    auto precedence = currentPrecedence();
    advance();
    auto right = parseExpression(precedence);
    return new InfixExpression(pToken, left, op, right);
}

Expression *Parser::parseCallExpression(Expression *name) {
    auto pToken = currentToken;
    std::vector<const Expression *> arguments;

    if (nextToken->type == kRParenToken) {
        advance();
        return new CallExpression(pToken, name, arguments);
    }

    advance();
    arguments.push_back(parseExpression());

    while (nextToken->type == kCommaToken) {
        advance();
        advance();
        arguments.push_back(parseExpression());
    }

    if (!expectNext(kRParenToken)) {
        registerError(name->sourceMark, "Missing ',' or ')'");
        delete name;
        for (const auto &argument:arguments) {
            delete argument;
        }
        return new EmptyNode();
    }

    return new CallExpression(pToken, name, arguments);
}
