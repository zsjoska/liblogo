
#include "Program.h"

Program::~Program() {
    for (const auto &statement:statements) {
        delete statement;
    }
}

void Program::print(std::ostream &os) {
    for (const auto &statement:statements) {
        statement->print(os, 0);
    }
}
