#pragma once

#include <vector>
#include <string>

#include "Lexer.h"
#include "Program.h"
#include "Node.h"
#include "Expression.h"
#include "WithErrors.h"

enum Precedence {
    kLowestPrecedence,
    kEqualsPrecedence,
    kLessGreaterPrecedence,
    kSumPrecedence,
    kProductPrecedence,
    kPrefixPrecedence,
    kCallPrecedence
};

class Parser;

typedef Expression *(Parser::*InfixParseFuncPtr)(Expression *);

class Parser : public WithErrors {

    Lexer &lexer;
    Token *currentToken;
    Token *nextToken;

    void advance();

    static Precedence mapPrecedence(TokenType type);

    Statement *parseStatement();

    Precedence nextPrecedence();

    Precedence currentPrecedence();

    bool expectNext(TokenType type);

    Expression *parseExpression(Precedence precedence = kLowestPrecedence);

    Expression *callPrefixFunction(TokenType type);

    Identifier *parseIdentifier();

    Variable *parseVariable();

    Expression *parseIfExpression();

    Expression *parseFunctionLiteral();

    Expression *parseRepeatExpression();

    Expression *parseBoolean();

    NumberLiteral *parseNumber();

    Expression *parsePrefixExpression();

    Expression *parseGroupedExpression();

    BlockStatement *parseBlockStatement();

    static InfixParseFuncPtr mapInfixFunction(TokenType type);

    Expression *parseInfixExpression(Expression *expression);

    Expression *parseCallExpression(Expression *expression);

public:
    explicit Parser(Lexer &lexer);

    ~Parser() override = default;

    Program *parse();
};
