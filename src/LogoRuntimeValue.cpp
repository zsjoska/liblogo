#include "LogoRuntimeValue.h"

LogoRuntimeValue::~LogoRuntimeValue() {
    if (ptr == nullptr) {
        return;
    }
    switch (type) {
        case kIntRuntimeType:
            delete (int *) ptr;
            break;
        case kBoolRuntimeType:
            delete (bool *) ptr;
            break;
        case kStrRuntimeType:
            delete[] (char *) ptr;
            break;
        case kDoubleRuntimeType:
            delete (double *) ptr;
            break;
        case kFuncRuntimeType:
            // don't delete the function
            break;
    }
}

int LogoRuntimeValue::getInt() {
    if (type == kIntRuntimeType) {
        return *((int *) ptr);
    }
    // conversion?
    return 0;
}

double LogoRuntimeValue::getDouble() {
    if (type == kDoubleRuntimeType) {
        return *((double *) ptr);
    }
    // conversion?
    return 0;
}

char *LogoRuntimeValue::getStr() {
    if (type == kStrRuntimeType) {
        return (char *) ptr;
    }
    // conversion?
    return nullptr;
}

bool LogoRuntimeValue::getBool() {
    if (type == kBoolRuntimeType) {
        return *((bool *) ptr);
    }
    // conversion?
    return false;
}

Function *LogoRuntimeValue::getFunction() {
    if (type == kFuncRuntimeType) {
        return static_cast<Function *>(ptr);
    }
    return nullptr;
}

LogoRuntimeValue::LogoRuntimeValue(LogoRuntimeValue *pValue) {
    type = pValue->type;
    if (pValue->isStr()) {
        ptr = copyStr(pValue->getStr());
    } else if (pValue->isBool()) {
        ptr = new bool{pValue->getBool()};
    } else if (pValue->isInt()) {
        ptr = new int{pValue->getInt()};
    } else if (pValue->isDouble()) {
        ptr = new double{pValue->getDouble()};
    } else if (pValue->isFunction()) {
        type = kStrRuntimeType;
        ptr = copyStr("WARN: Copy of function shouldn't happen");
    }
}
