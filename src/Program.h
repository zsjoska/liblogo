#pragma once

#include <utility>
#include <vector>
#include "Node.h"
#include "WithErrors.h"

class Program : public WithErrors {
    std::vector<const Statement *> statements;
public:
    explicit Program(std::vector<const Statement *> statements) :
            statements(std::move(statements)) {}

    ~Program() override;

    void print(std::ostream &os);

    std::vector<const Statement *> &getStatements() {
        return statements;
    }
};
