#include "Expression.h"

std::vector<const Expression *> Expression::copyExpressions(const std::vector<const Expression *> &expressions) {
    std::vector<const Expression *> expressionsCopy;
    for (const auto &expression:expressions) {
        expressionsCopy.push_back(expression->copy());
    }
    return expressionsCopy;
}

void EmptyNode::print(std::ostream &os, int indent) const {
    os << std::setw(indent * 2) << ">";
    os << "EmptyNode" << std::endl;
}

Identifier::~Identifier() {
    delete[] literal;
}

void Identifier::print(std::ostream &os, int indent) const {
    os << std::setw(indent * 2) << ">";
    os << "Identifier: " << literal << std::endl;
}

void BooleanLiteral::print(std::ostream &os, int indent) const {
    os << std::setw(indent * 2) << ">";
    os << "BooleanLiteral: " << value << std::endl;
}

std::vector<const Variable *> Variable::copyVariables(const std::vector<const Variable *> &variables) {
    std::vector<const Variable *> variablesCopy;
    for (const auto &variable:variables) {
        variablesCopy.push_back(variable->copy());
    }
    return variablesCopy;
}

Variable::~Variable() {
    delete[] literal;
}

void Variable::print(std::ostream &os, int indent) const {
    os << std::setw(indent * 2) << ">";
    os << "Variable: " << literal << std::endl;
}

void NumberLiteral::print(std::ostream &os, int indent)const {
    os << std::setw(indent * 2) << ">";
    os << "NumberLiteral:" << value << std::endl;
}

ExpressionStatement::~ExpressionStatement() {
    delete expression;
}

void ExpressionStatement::print(std::ostream &os, int indent) const{
    os << std::setw(indent * 2) << ">";
    os << "ExpressionStatement:" << std::endl;
    expression->print(os, indent + 1);
}

IfExpression::~IfExpression() {
    delete condition;
    delete consequence;
    delete alternative;
}

void IfExpression::print(std::ostream &os, int indent) const{
    os << std::setw(indent * 2) << ">";
    os << "IfExpression:" << std::endl;
    condition->print(os, indent + 1);
    consequence->print(os, indent + 1);
    if (alternative != nullptr) {
        alternative->print(os, indent + 1);
    }
}

FunctionLiteral::~FunctionLiteral() {
    delete identifier;
    for (const auto &variable:variables) {
        delete variable;
    }
    delete body;
}

void FunctionLiteral::print(std::ostream &os, int indent)const {
    os << std::setw(indent * 2) << ">";
    os << "FunctionLiteral:" << std::endl;
    identifier->print(os, indent + 1);
    for (const auto &variable:variables) {
        variable->print(os, indent + 1);
    }
    body->print(os, indent + 1);
}

RepeatExpression::~RepeatExpression() {
    delete count;
    delete statement;
}

void RepeatExpression::print(std::ostream &os, int indent)const {
    os << std::setw(indent * 2) << ">";
    os << "RepeatExpression:" << std::endl;
    count->print(os, indent + 1);
    statement->print(os, indent + 1);
}

PrefixExpression::~PrefixExpression() {
    delete[] op;
    delete right;
}

void PrefixExpression::print(std::ostream &os, int indent)const {
    os << std::setw(indent * 2) << ">";
    os << "PrefixExpression:" << op << std::endl;
    right->print(os, indent + 1);
}

InfixExpression::~InfixExpression() {
    delete left;
    delete[] op;
    delete right;
}

void InfixExpression::print(std::ostream &os, int indent)const {
    os << std::setw(indent * 2) << ">";
    os << "InfixExpression:" << op << std::endl;
    left->print(os, indent + 1);
    right->print(os, indent + 1);
}

CallExpression::~CallExpression() {
    delete name;
    for (const auto &argument:arguments) {
        delete argument;
    }
}

void CallExpression::print(std::ostream &os, int indent) const{
    os << std::setw(indent * 2) << ">";
    os << "CallExpression:" << std::endl;
    name->print(os, indent + 1);
    for (const auto &argument:arguments) {
        argument->print(os, indent + 1);
    }
}
