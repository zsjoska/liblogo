#pragma once

#include "LogoRuntime.h"

class LogoInterpreter {
    LogoRuntime *runtime;

public:
    LogoInterpreter() : runtime(new LogoRuntime) {}

    ~LogoInterpreter() { delete runtime; }

    LogoRuntime *getRuntime() { return runtime; }

    bool execute(const char *code);

    static Program *parse(const char *code);

    void registerBuiltInFunc(const char *name, BuiltInFuncPtr ptr) {
        runtime->registerBuiltInFunc(name, ptr);
    }

    std::string asOneError() { return runtime->asOneError(); }

    bool hasErrors() { return runtime->hasErrors(); }
};
