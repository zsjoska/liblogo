#pragma once

#include <cstring>
#include <iostream>
#include <utility>

#include "Lexer.h"
#include "Node.h"
#include "utils.h"

class Expression : public Node {
protected:
    static std::vector<const Expression *> copyExpressions(const std::vector<const Expression *> &expressions);

    Expression(LogoNode logoNode, SourceMark sourceMark) :
            Node(logoNode, sourceMark) {}

public:
    Expression *copy() const override { return nullptr; }
};

class EmptyNode : public Expression {
protected:
    explicit EmptyNode(const EmptyNode *pNode) : Expression(pNode->logoNode, pNode->sourceMark) {}

public:
    EmptyNode() :
            Expression(kEmptyNodeLogoNode, SourceMark{0, 0}) {}

    void print(std::ostream &os, int indent) const override;

    EmptyNode *copy() const override { return new EmptyNode(this); }
};

class Identifier : public Expression {
    const char *literal;
protected:
    explicit Identifier(const Identifier *pIdentifier) : Expression(pIdentifier->logoNode, pIdentifier->sourceMark),
                                                         literal(copyStr(pIdentifier->literal)) {}

public:
    Identifier(Token *pToken, const char *literal) :
            Expression(kIdentifierLogoNode, pToken->sourceMark), literal(copyStr(literal)) {}

    ~Identifier() override;

    void print(std::ostream &os, int indent) const override;

    Identifier *copy() const override { return new Identifier(this); }

    const char *getLiteral() const {
        return literal;
    }
};

class BooleanLiteral : public Expression {
protected:
    explicit BooleanLiteral(const BooleanLiteral *pLiteral) : Expression(pLiteral->logoNode, pLiteral->sourceMark),
                                                              value(pLiteral->value) {}

public:

    const bool value;

    BooleanLiteral(Token *pToken, bool value) :
            Expression(kBooleanLiteralLogoNode, pToken->sourceMark), value(value) {}

    void print(std::ostream &os, int indent) const override;

    BooleanLiteral *copy() const override { return new BooleanLiteral(this); }
};

class Variable : public Expression {

    const char *literal;
protected:
    explicit Variable(const Variable *pVariable) : Expression(pVariable->logoNode, pVariable->sourceMark),
                                                   literal(copyStr(pVariable->literal)) {}

public:
    static std::vector<const Variable *> copyVariables(const std::vector<const Variable *> &variables);

    Variable(Token *pToken, const char *literal) :
            Expression(kVariableLogoNode, pToken->sourceMark), literal(copyStr(literal)) {}

    ~Variable() override;

    void print(std::ostream &os, int indent) const override;

    Variable *copy() const override { return new Variable(this); }

    const char *getLiteral() const {
        return literal;
    }
};

class NumberLiteral : public Expression {
protected:
    explicit NumberLiteral(const NumberLiteral *pLiteral) : Expression(pLiteral->logoNode, pLiteral->sourceMark),
                                                            value(pLiteral->value) {}

public:

    const double value;

    NumberLiteral(Token *pToken, double value) :
            Expression(kNumberLiteralLogoNode, pToken->sourceMark), value(value) {}

    void print(std::ostream &os, int indent) const override;

    NumberLiteral *copy() const override { return new NumberLiteral(this); }
};

class ExpressionStatement : public Statement {

    Expression *expression;
protected:
    explicit ExpressionStatement(const ExpressionStatement *pStatement) : Statement(pStatement->logoNode,
                                                                                    pStatement->sourceMark),
                                                                          expression(pStatement->expression->copy()) {}

public:
    ExpressionStatement(Token *pToken, Expression *expression) :
            Statement(kExprStatementLogoNode, pToken->sourceMark), expression(expression) {}

    ~ExpressionStatement() override;

    void print(std::ostream &os, int indent) const override;

    ExpressionStatement *copy() const override { return new ExpressionStatement(this); }


    Expression *getExpression() const {
        return expression;
    }
};

class IfExpression : public Expression {

    const Expression *condition;
    const BlockStatement *consequence;
    const BlockStatement *alternative;
protected:
    explicit IfExpression(const IfExpression *pExpression) : Expression(pExpression->logoNode, pExpression->sourceMark),
                                                             condition(pExpression->condition->copy()),
                                                             consequence(pExpression->consequence->copy()),
                                                             alternative(pExpression->alternative != nullptr
                                                                         ? pExpression->alternative->copy()
                                                                         : nullptr) {}

public:
    IfExpression(Token *pToken, Expression *expression,
                 BlockStatement *consequence, BlockStatement *alternative) :
            Expression(kIfExprLogoNode, pToken->sourceMark), condition(expression), consequence(
            consequence), alternative(alternative) {}

    ~IfExpression() override;

    void print(std::ostream &os, int indent) const override;

    IfExpression *copy() const override { return new IfExpression(this); }


    const Expression *getCondition() const {
        return condition;
    }

    const BlockStatement *getConsequence() const {
        return consequence;
    }

    const BlockStatement *getAlternative() const {
        return alternative;
    }
};

class FunctionLiteral : public Expression {

    const Identifier *identifier;
    const std::vector<const Variable *> variables;
    const BlockStatement *body;
protected:
    FunctionLiteral(const FunctionLiteral *pLiteral) : Expression(pLiteral->logoNode, pLiteral->sourceMark),
                                                       identifier(pLiteral->identifier->copy()),
                                                       variables(Variable::copyVariables(pLiteral->variables)),
                                                       body(pLiteral->body->copy()) {}

public:
    FunctionLiteral(Token *pToken, Identifier *identifier,
                    const std::vector<const Variable *> variables, const BlockStatement *body) :
            Expression(kFuncLiteralLogoNode, pToken->sourceMark), identifier(identifier), variables(variables),
            body(body) {}

    ~FunctionLiteral() override;

    void print(std::ostream &os, int indent) const override;

    FunctionLiteral *copy() const override { return new FunctionLiteral(this); }

    const Identifier *getName() const { return identifier; }

    const std::vector<const Variable *> &getArguments() const { return variables; }

    const BlockStatement *getBody() const { return body; }
};

class RepeatExpression : public Expression {

    const NumberLiteral *count;
    const BlockStatement *statement;
protected:
    RepeatExpression(const RepeatExpression *pExpression) : Expression(pExpression->logoNode, pExpression->sourceMark),
                                                            count(pExpression->count->copy()),
                                                            statement(pExpression->statement->copy()) {}

public:
    RepeatExpression(Token *pToken, NumberLiteral *count,
                     BlockStatement *statement) :
            Expression(kRepeatExprLogoNode, pToken->sourceMark), count(count), statement(
            statement) {}

    ~RepeatExpression() override;

    void print(std::ostream &os, int indent) const override;

    RepeatExpression *copy() const override { return new RepeatExpression(this); }

    const NumberLiteral *getCount() const {
        return count;
    }

    const BlockStatement *getStatement() const {
        return statement;
    }
};

class PrefixExpression : public Expression {

    const char *op;
    const Expression *right;
protected:
    PrefixExpression(const PrefixExpression *pExpression) : Expression(pExpression->logoNode, pExpression->sourceMark),
                                                            op(copyStr(pExpression->op)),
                                                            right(pExpression->right->copy()) {}

public:
    PrefixExpression(Token *pToken, const char *op, const Expression *right) :
            Expression(kPrefixExprLogoNode, pToken->sourceMark), op(copyStr(op)), right(
            right) {}

    ~PrefixExpression() override;

    void print(std::ostream &os, int indent) const override;

    PrefixExpression *copy() const override { return new PrefixExpression(this); }

    const Expression *getRight() {
        return right;
    }

    const char *getOp() {
        return op;
    }
};

class InfixExpression : public Expression {

    const Expression *left;
    const char *op;
    const Expression *right;
protected:
    InfixExpression(const InfixExpression *pExpression) : Expression(pExpression->logoNode, pExpression->sourceMark),
                                                          left(pExpression->left->copy()), op(copyStr(pExpression->op)),
                                                          right(pExpression->right->copy()) {}

public:
    InfixExpression(Token *pToken, Expression *left, const char *op,
                    Expression *right) :
            Expression(kInfixExprLogoNode, pToken->sourceMark), left(left), op(
            copyStr(op)), right(right) {}

    ~InfixExpression() override;

    void print(std::ostream &os, int indent) const override;

    InfixExpression *copy() const override { return new InfixExpression(this); }

    const Expression *getLeft() {
        return left;
    }

    const Expression *getRight() {
        return right;
    }

    const char *getOp() {
        return op;
    }
};

class CallExpression : public Expression {

    const Expression *name;
    const std::vector<const Expression *> arguments;
protected:
    CallExpression(const CallExpression *pExpression) : Expression(pExpression->logoNode, pExpression->sourceMark),
                                                        name(pExpression->name->copy()),
                                                        arguments(copyExpressions(pExpression->arguments)) {}

public:
    CallExpression(Token *pToken, const Expression *name,
                   const std::vector<const Expression *> arguments) :
            Expression(kCallExprLogoNode, pToken->sourceMark), name(name), arguments(arguments) {}

    ~CallExpression() override;

    void print(std::ostream &os, int indent) const override;

    CallExpression *copy() const override { return new CallExpression(this); }

    std::vector<const Expression *> getArguments() const {
        return arguments;
    }

    const Expression *getName() const {
        return name;
    }
};
