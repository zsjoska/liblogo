#pragma once

#include <vector>
#include <string>
#include "Token.h"

class WithErrors {
    std::vector<std::string> errors;
protected:
    WithErrors() = default;

    virtual ~WithErrors() = default;

    void registerError(SourceMark sourceMark, const char *format, ...);

public:
    std::vector<std::string> &getParseErrors() {
        return this->errors;
    }

    std::string asOneError();

    bool hasErrors() {
        return !errors.empty();
    }

    void importErrors(std::vector<std::string> otherErrors) {
        errors.insert(errors.end(), otherErrors.begin(), otherErrors.end());
    }

    void clearErrors() {
        errors.clear();
    }
};
