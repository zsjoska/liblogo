#pragma once

#include <vector>
#include <iomanip>
#include <ostream>
#include <iostream>

#include "utils.h"

enum TokenType {
    kIdentifierToken,
    kToToken,
    kEndToken,
    kVariableToken,
    kNumberToken,
    kStringToken,
    kIfToken,
    kElseToken,
    kRepeatToken,
    kTrueToken,
    kFalseToken,
    kLBracketToken,
    kRBracketToken,
    kLParenToken,
    kRParenToken,
    kPlusToken,
    kMinusToken,
    kAsterixToken,
    kSlashToken,
    kEqualsToken,
    kNotEqualsToken,
    kLTToken,
    kGTToken,
    kIllegalToken,
    kCommaToken,
    kEofToken
};

struct SourceMark {
    const unsigned int line;
    const unsigned int column;
};

class Token {
public:
    const TokenType type;
    const SourceMark sourceMark;
    const char *literal;

    Token(TokenType type, SourceMark sourceMark, const char *literal);

    Token(TokenType type, SourceMark sourceMark, char c);

    virtual ~Token();

    void print(std::ostream &os) const {
        os << "[" << sourceMark.line << "," << sourceMark.column << "]#" << type << ":"
           << (literal != nullptr ? literal : "") << std::endl;
    }
};

