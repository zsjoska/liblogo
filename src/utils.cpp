#include "utils.h"
#include <cstring>

char *copyStr(const char *original) {
    if (original == nullptr) return nullptr;
    auto dest = new char[strlen(original) + 1];
    strcpy(dest, original);
    return dest;
}
