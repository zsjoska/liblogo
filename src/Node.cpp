#include "Node.h"

std::vector<const Statement *> Statement::copyStatements(const std::vector<const Statement *> &statements) {
    std::vector<const Statement *> statementsCopy;
    for (const auto &statement:statements) {
        statementsCopy.push_back(statement->copy());
    }
    return statementsCopy;
}

BlockStatement::~BlockStatement() {
    for (const auto &statement:statements) {
        delete statement;
    }
}

void BlockStatement::print(std::ostream &os, int indent) const {
    os << std::setw(indent * 2) << ">";
    os << "BlockStatement:" << std::endl;
    for (const auto &statement:statements) {
        statement->print(os, indent + 1);
    }
}
