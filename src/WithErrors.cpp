#include <cstdarg>
#include <sstream>
#include <cstdio>

#include "WithErrors.h"

void WithErrors::registerError(SourceMark sourceMark, const char *format,
                               ...) {
    char buff[200];
    snprintf(buff, sizeof(buff), "[%d,%d]:", sourceMark.line, sourceMark.column);
    std::string str(buff);

    std::va_list args;
    va_start(args, format);
    vsnprintf(buff, sizeof(buff), format, args);
    va_end(args);
    str.append(buff);

    errors.push_back(str);
}

std::string WithErrors::asOneError() {
    std::ostringstream oss;
    for (const auto &error:errors) {
        oss << error << std::endl;
    }
    return oss.str();
}
