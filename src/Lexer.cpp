#include "Lexer.h"
#include <cctype>
#include <cstring>
#include <iostream>

Lexer::Lexer(const char *code) :
        code(code), currentPosition(0) {
    currentChar = code[currentPosition];
}

Lexer::~Lexer() {
    clearTokens();
}

void Lexer::clearTokens() {
    for (const auto &token:tokens) {
        delete token;
    }
    tokens.clear();
}

std::vector<Token *> Lexer::lex() {
    clearTokens();
    Token *token;
    do {
        token = nextToken();
    } while (token != nullptr && token->type != kEofToken);

    return tokens;
}

Token *Lexer::nextToken() {
    auto token = parseNextToken();
    tokens.push_back(token);
    return token;
}

Token *Lexer::parseNextToken() {
    Token *token;

    skipWhitespace();

    switch (currentChar) {
        case '[':
            token = new Token(kLBracketToken, SourceMark{source_line, source_column}, currentChar);
            break;
        case ']':
            token = new Token(kRBracketToken, SourceMark{source_line, source_column}, currentChar);
            break;
        case '(':
            token = new Token(kLParenToken, SourceMark{source_line, source_column}, currentChar);
            break;
        case ')':
            token = new Token(kRParenToken, SourceMark{source_line, source_column}, currentChar);
            break;
        case '+':
            token = new Token(kPlusToken, SourceMark{source_line, source_column}, currentChar);
            break;
        case '-':
            token = new Token(kMinusToken, SourceMark{source_line, source_column}, currentChar);
            break;
        case '*':
            token = new Token(kAsterixToken, SourceMark{source_line, source_column}, currentChar);
            break;
        case '<':
            token = new Token(kLTToken, SourceMark{source_line, source_column}, currentChar);
            break;
        case '>':
            token = new Token(kGTToken, SourceMark{source_line, source_column}, currentChar);
            break;
        case '/':
            token = new Token(kSlashToken, SourceMark{source_line, source_column}, currentChar);
            break;
        case '=':
            token = new Token(kEqualsToken, SourceMark{source_line, source_column}, currentChar);
            break;
        case ',':
            token = new Token(kCommaToken, SourceMark{source_line, source_column}, currentChar);
            break;
        case '!':
            if (nextChar() == '=') {
                readChar();
                token = new Token(kNotEqualsToken, SourceMark{source_line, source_column}, copyStr("!="));
            } else {
                token = new Token(kIllegalToken, SourceMark{source_line, source_column}, nullptr);
            }
            break;
        case ':':
            readChar();
            return new Token(kVariableToken, SourceMark{source_line, source_column}, readString());
        case 10:
        case 0:
            token = new Token(kEofToken, SourceMark{source_line, source_column}, nullptr);
            break;

        default:
            if (isalpha(currentChar)) {
                auto str = readString();
                auto type = mapKeyword(str);
                if (type == kIllegalToken) {
                    type = kIdentifierToken;
                }
                return new Token(type, SourceMark{source_line, source_column}, str);
            } else if (isdigit(currentChar)) {
                return new Token(kNumberToken, SourceMark{source_line, source_column}, readNumber());
            } else {
                token = new Token(kIllegalToken, SourceMark{source_line, source_column}, currentChar);
            }
            break;
    }

    readChar();
    return token;
}

void Lexer::skipWhitespace() {
    while (isspace(currentChar))
        readChar();
}

void Lexer::readChar() {
    if (currentChar == '\n') {
        source_column = 0;
        source_line++;
    }
    source_column++;

    auto nextPosition = currentPosition + 1;
    currentChar = (nextPosition >= strlen(code)) ? ('\0') : code[nextPosition];
    currentPosition = nextPosition;
}

char Lexer::nextChar() {
    auto nextPosition = currentPosition + 1;
    if (nextPosition >= strlen(code)) {
        return 0;
    }
    return code[nextPosition];
}

char *Lexer::readString() {
    auto startPosition = currentPosition;

    while (isalpha(currentChar)) {
        readChar();
    }

    auto str = new char[currentPosition - startPosition + 1];
    strncpy(str, code + startPosition, currentPosition - startPosition);
    str[currentPosition - startPosition] = '\0';
    return str;
}

char *Lexer::readNumber() {
    auto startPosition = currentPosition;

    while (isdigit(currentChar)) {
        readChar();
    }

    auto str = new char[currentPosition - startPosition + 1];
    strncpy(str, code + startPosition, currentPosition - startPosition);
    str[currentPosition - startPosition] = '\0';
    return str;
}

TokenType Lexer::mapKeyword(const char *token) {
    if (strcmp(token, "to") == 0)
        return kToToken;
    if (strcmp(token, "end") == 0)
        return kEndToken;
    if (strcmp(token, "if") == 0)
        return kIfToken;
    if (strcmp(token, "else") == 0)
        return kElseToken;
    if (strcmp(token, "true") == 0)
        return kTrueToken;
    if (strcmp(token, "false") == 0)
        return kFalseToken;
    if (strcmp(token, "repeat") == 0)
        return kRepeatToken;
    return kIllegalToken;
}
