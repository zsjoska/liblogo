#pragma once

#include <iomanip>
#include <ostream>

#include "Lexer.h"

enum LogoNode {
    kEmptyNodeLogoNode,
    kBlockStatementLogoNode,
    kIdentifierLogoNode,
    kVariableLogoNode,
    kExprStatementLogoNode,
    kBooleanLiteralLogoNode,
    kNumberLiteralLogoNode,
    kFuncLiteralLogoNode,
    kIfExprLogoNode,
    kRepeatExprLogoNode,
    kPrefixExprLogoNode,
    kInfixExprLogoNode,
    kCallExprLogoNode,
};

class Node {
public:
    const LogoNode logoNode;
    const SourceMark sourceMark;

    virtual ~Node() = default;

    virtual void print(std::ostream &os, int indent) const = 0;

    virtual Node *copy() const = 0;

protected:
    Node(LogoNode logoNode, SourceMark sourceMark) :
            logoNode(logoNode), sourceMark(sourceMark) {}
};

class Statement : public Node {
protected:
    Statement(LogoNode logoNode, SourceMark sourceMark) :
            Node(logoNode, sourceMark) {}

    static std::vector<const Statement *> copyStatements(const std::vector<const Statement *> &statements);

public:
    Statement *copy() const override = 0;
};

class BlockStatement : public Statement {
    BlockStatement(const BlockStatement *pStatement) : Statement(pStatement->logoNode,
                                                                 pStatement->sourceMark),
                                                       statements(copyStatements(pStatement->statements)) {}

    const std::vector<const Statement *> statements;
public:
    BlockStatement(Token *pToken, std::vector<const Statement *> &statements) :
            Statement(kBlockStatementLogoNode, pToken->sourceMark), statements(statements) {}

    ~BlockStatement() override;

    const std::vector<const Statement *> &getStatements() const {
        return statements;
    }

    void print(std::ostream &os, int indent) const override;

    BlockStatement *copy() const override { return new BlockStatement(this); }
};
