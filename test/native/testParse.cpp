#include <unity.h>
#include <sstream>
#include "testdefs.h"
#include "Parser.h"

void test_parse_simple_fd() {
    std::ostringstream oss;
    auto code = "fd(50)";
    auto expected = ">ExpressionStatement:\n >CallExpression:\n   >Identifier: fd\n   >NumberLiteral:50\n";

    Lexer lexer(code);
    Parser parser(lexer);
    auto program = parser.parse();

    auto errors = parser.getParseErrors();
    TEST_ASSERT_EQUAL_MESSAGE(0, errors.size(), code);

    program->print(oss);
    TEST_ASSERT_EQUAL_STRING_MESSAGE(expected, oss.str().c_str(), code);

    delete program;
}

void test_parse_square() {
    std::ostringstream oss;
    auto code = "repeat 4 [fd(100) rt(90)]";
    auto expected = ">ExpressionStatement:\n"
                    " >RepeatExpression:\n"
                    "   >NumberLiteral:4\n"
                    "   >BlockStatement:\n"
                    "     >ExpressionStatement:\n"
                    "       >CallExpression:\n"
                    "         >Identifier: fd\n"
                    "         >NumberLiteral:100\n"
                    "     >ExpressionStatement:\n"
                    "       >CallExpression:\n"
                    "         >Identifier: rt\n"
                    "         >NumberLiteral:90\n";

    Lexer lexer(code);
    Parser parser(lexer);
    auto program = parser.parse();

    auto errors = parser.getParseErrors();
    TEST_ASSERT_EQUAL_MESSAGE(0, errors.size(), code);

    program->print(oss);
    TEST_ASSERT_EQUAL_STRING_MESSAGE(expected, oss.str().c_str(), code);

    delete program;
}

void test_parse_tree() {
    std::ostringstream oss;
    auto code = "to tree :size\n"
                "  if (:size < 5) [\n"
                "    fd(:size) bk(:size)\n"
                "  ] else [\n"
                "    fd(:size/3)  \n"
                "    lt(30) tree(:size*2/3) rt(30)\n"
                "    fd(:size/6)\n"
                "    rt(25) tree(:size/2) lt(25)\n"
                "    fd(:size/3)\n"
                "    rt(25) tree(:size/2) lt(25)\n"
                "    fd(:size/6)\n"
                "    bk(:size)\n"
                "  ]\n"
                "end\n"
                "\n"
                "tree(350)";
    auto expected = ">ExpressionStatement:\n"
                    " >FunctionLiteral:\n"
                    "   >Identifier: tree\n"
                    "   >Variable: size\n"
                    "   >BlockStatement:\n"
                    "     >ExpressionStatement:\n"
                    "       >IfExpression:\n"
                    "         >InfixExpression:<\n"
                    "           >Variable: size\n"
                    "           >NumberLiteral:5\n"
                    "         >BlockStatement:\n"
                    "           >ExpressionStatement:\n"
                    "             >CallExpression:\n"
                    "               >Identifier: fd\n"
                    "               >Variable: size\n"
                    "           >ExpressionStatement:\n"
                    "             >CallExpression:\n"
                    "               >Identifier: bk\n"
                    "               >Variable: size\n"
                    "         >BlockStatement:\n"
                    "           >ExpressionStatement:\n"
                    "             >CallExpression:\n"
                    "               >Identifier: fd\n"
                    "               >InfixExpression:/\n"
                    "                 >Variable: size\n"
                    "                 >NumberLiteral:3\n"
                    "           >ExpressionStatement:\n"
                    "             >CallExpression:\n"
                    "               >Identifier: lt\n"
                    "               >NumberLiteral:30\n"
                    "           >ExpressionStatement:\n"
                    "             >CallExpression:\n"
                    "               >Identifier: tree\n"
                    "               >InfixExpression:/\n"
                    "                 >InfixExpression:*\n"
                    "                   >Variable: size\n"
                    "                   >NumberLiteral:2\n"
                    "                 >NumberLiteral:3\n"
                    "           >ExpressionStatement:\n"
                    "             >CallExpression:\n"
                    "               >Identifier: rt\n"
                    "               >NumberLiteral:30\n"
                    "           >ExpressionStatement:\n"
                    "             >CallExpression:\n"
                    "               >Identifier: fd\n"
                    "               >InfixExpression:/\n"
                    "                 >Variable: size\n"
                    "                 >NumberLiteral:6\n"
                    "           >ExpressionStatement:\n"
                    "             >CallExpression:\n"
                    "               >Identifier: rt\n"
                    "               >NumberLiteral:25\n"
                    "           >ExpressionStatement:\n"
                    "             >CallExpression:\n"
                    "               >Identifier: tree\n"
                    "               >InfixExpression:/\n"
                    "                 >Variable: size\n"
                    "                 >NumberLiteral:2\n"
                    "           >ExpressionStatement:\n"
                    "             >CallExpression:\n"
                    "               >Identifier: lt\n"
                    "               >NumberLiteral:25\n"
                    "           >ExpressionStatement:\n"
                    "             >CallExpression:\n"
                    "               >Identifier: fd\n"
                    "               >InfixExpression:/\n"
                    "                 >Variable: size\n"
                    "                 >NumberLiteral:3\n"
                    "           >ExpressionStatement:\n"
                    "             >CallExpression:\n"
                    "               >Identifier: rt\n"
                    "               >NumberLiteral:25\n"
                    "           >ExpressionStatement:\n"
                    "             >CallExpression:\n"
                    "               >Identifier: tree\n"
                    "               >InfixExpression:/\n"
                    "                 >Variable: size\n"
                    "                 >NumberLiteral:2\n"
                    "           >ExpressionStatement:\n"
                    "             >CallExpression:\n"
                    "               >Identifier: lt\n"
                    "               >NumberLiteral:25\n"
                    "           >ExpressionStatement:\n"
                    "             >CallExpression:\n"
                    "               >Identifier: fd\n"
                    "               >InfixExpression:/\n"
                    "                 >Variable: size\n"
                    "                 >NumberLiteral:6\n"
                    "           >ExpressionStatement:\n"
                    "             >CallExpression:\n"
                    "               >Identifier: bk\n"
                    "               >Variable: size\n"
                    ">ExpressionStatement:\n"
                    " >CallExpression:\n"
                    "   >Identifier: tree\n"
                    "   >NumberLiteral:350\n";

    Lexer lexer(code);
    Parser parser(lexer);
    auto program = parser.parse();

    auto errors = parser.getParseErrors();
    TEST_ASSERT_EQUAL_MESSAGE(0, errors.size(), code);

    program->print(oss);
    TEST_ASSERT_EQUAL_STRING_MESSAGE(expected, oss.str().c_str(), code);

    delete program;
}

void test_parse_error_() {
    std::ostringstream oss;
    auto code = "fd(50bk)";
    auto expected = "[1,7]:Unexpected token 'bk'\n[1,2]:Missing ',' or ')'\n[1,7]:no prefix parse function for )\n";

    Lexer lexer(code);
    Parser parser(lexer);
    auto program = parser.parse();

    auto errors = parser.getParseErrors();
    TEST_ASSERT_EQUAL_MESSAGE(3, errors.size(), code);

    TEST_ASSERT_EQUAL_STRING_MESSAGE(expected, parser.asOneError().c_str(), code);

    delete program;
}

void test_parse_error_missing_end() {
    std::ostringstream oss;
    auto code = "to func :param fd(50)";
    auto expected = "[1,2]:Missing end for procedure\n";

    Lexer lexer(code);
    Parser parser(lexer);
    auto program = parser.parse();

    auto errors = parser.getParseErrors();
    TEST_ASSERT_EQUAL_MESSAGE(1, errors.size(), code);

    TEST_ASSERT_EQUAL_STRING_MESSAGE(expected, parser.asOneError().c_str(), code);

    delete program;
}

void run_test_parse() {
    UNITY_BEGIN();

    RUN_TEST(test_parse_simple_fd);
    RUN_TEST(test_parse_square);
    RUN_TEST(test_parse_tree);
    RUN_TEST(test_parse_error_);
    RUN_TEST(test_parse_error_missing_end);

    UNITY_END();
}
