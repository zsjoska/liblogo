#include <unity.h>
#include <stdio.h>
#include <sstream>
#include "LogoInterpreter.h"
#include "LogoRuntimeValue.h"
#include "testdefs.h"

std::string bulktests[][3] = {
        {"fd(50)",                                     "Forward: 50\n",                              ""},
        {"fd(-50)",                                    "Forward: -50\n",                             ""},
        {"bool(1>2)",                                  "bool: 0\n",                                  ""},
        {"fd(50) bk(20)",                              "Forward: 50\nBackward: 20\n",                ""},
        {"repeat 2 [fd(50)]",                          "Forward: 50\nForward: 50\n",                 ""},
        {"fd(50/2)",                                   "Forward: 25\n",                              ""},
        {"fd(25+25)",                                  "Forward: 50\n",                              ""},
        {"fd(10+20*2)",                                "Forward: 50\n",                              ""},
        {"fd(2+2*2-2*2)",                              "Forward: 2\n",                               ""},
        {"fd(50-25)",                                  "Forward: 25\n",                              ""},
        {"fd(2*25)",                                   "Forward: 50\n",                              ""},
        {"if( 1 < 2) [ fd(50) ] else [ bk(50) ]",      "Forward: 50\n",                              ""},
        {"if( 1 > 2) [ fd(50) ] else [ bk(50) ]",      "Backward: 50\n",                             ""},
        {"if( 1 > 2) [ fd(50) ]",                      "",                                           ""},
        {"if(false) [ fd(50) ]",                       "",                                           ""},
        {"if(true) [ fd(50) ]",                        "Forward: 50\n",                              ""},
        {"if(bool(false)) [ fd(50) ]",                 "bool: 0\nForward: 50\n",                     ""},
        {"to myfunc fd(50) end myfunc()",              "Forward: 50\n",                              ""},
        {"to myfunc fd(50) end myfunc() bk(50)",       "Forward: 50\nBackward: 50\n",                ""},
        {"to myfunc end myfunc()",                     "",                                           ""},
        {"to myfunc(50) end myfunc()",                 "",                                           ""},
        {"to myfunc :param fd(50) end myfunc(10)",     "Forward: 50\n",                              ""},
        {"to myfunc :param fd(:param) end myfunc(10)", "Forward: 10\n",                              ""},
        {"to rekurziv :param\n"
         "fd(:param)\n"
         "if(:param > 10) [rekurziv(:param-10)]\n"
         "bk(:param)\n"
         "end\n"
         "rekurziv(30)",                               "Forward: 30\nForward: 20\nForward: 10\n"
                                                       "Backward: 10\nBackward: 20\nBackward: 30\n", ""},
};

int bulk_rows = sizeof bulktests / sizeof bulktests[0];
int bulk_test_index = 0;

std::ostringstream bulk_oss;

LogoRuntimeValue *test_bulk_logo_fd(std::vector<LogoRuntimeValue *> &params) {
    auto param1 = params[0]->getDouble();
    bulk_oss << "Forward: " << param1 << std::endl;
    return new IntValue(216);
}

LogoRuntimeValue *test_bulk_logo_bk(std::vector<LogoRuntimeValue *> &params) {
    auto param1 = params[0]->getDouble();
    bulk_oss << "Backward: " << param1 << std::endl;
    return new IntValue(215);
}

LogoRuntimeValue *test_bulk_logo_bool(std::vector<LogoRuntimeValue *> &params) {
    auto param1 = params[0]->getBool();
    bulk_oss << "bool: " << param1 << std::endl;
    return new BoolValue(true);
}

void run_bulk_test() {
    bulk_oss.str("");
    bulk_oss.clear();

    const char *code = bulktests[bulk_test_index][0].c_str();
    const char *expectedExec = bulktests[bulk_test_index][1].c_str();
    const char *expectedError = bulktests[bulk_test_index][2].c_str();
    auto error1 = bulktests[bulk_test_index][0] + " Should not fail";
    auto error2 = bulktests[bulk_test_index][0] + " Error should be empty";
    auto error3 = bulktests[bulk_test_index][0] + " Should run";
    std::string error = "";
    bool hasError = false;
    std::cout << code << std::endl;
    {
        LogoInterpreter interpreter;
        interpreter.registerBuiltInFunc("fd", &test_bulk_logo_fd);
        interpreter.registerBuiltInFunc("bk", &test_bulk_logo_bk);
        interpreter.registerBuiltInFunc("bool", &test_bulk_logo_bool);
        interpreter.execute(code);
        hasError = interpreter.hasErrors();
        if (hasError) {
            error = interpreter.asOneError();
        }
    }

    TEST_ASSERT_TRUE_MESSAGE(!hasError, error.c_str());
    TEST_ASSERT_EQUAL_STRING_MESSAGE(expectedError, error.c_str(), error2.c_str());
    TEST_ASSERT_EQUAL_STRING_MESSAGE(expectedExec, bulk_oss.str().c_str(), error3.c_str());
    bulk_oss.str("");
    bulk_oss.clear();
}

void run_test_bulks() {
    UNITY_BEGIN();
    for (bulk_test_index = 0; bulk_test_index < bulk_rows; ++bulk_test_index) {
        RUN_TEST(run_bulk_test);
    }
    UNITY_END();
}
