#include <unity.h>
#include <stdio.h>
#include <sstream>
#include "Lexer.h"
#include "testdefs.h"

void test_lex_simple_fd() {
    std::ostringstream oss;
    auto expected = "[1,2]#0:fd\n[1,2]#13:(\n[1,3]#4:50\n[1,5]#14:)\n[1,6]#25:\n";
    auto code = "fd(50)";

    Lexer lexer(code);
    auto tokens = lexer.lex();
    for (const auto &token:tokens) {
        token->print(oss);
    }

    TEST_ASSERT_EQUAL_MESSAGE(5, tokens.size(), code);
    TEST_ASSERT_EQUAL_STRING_MESSAGE(expected, oss.str().c_str(), code);
}

void test_lex_multiline_fd() {
    std::ostringstream oss;

    auto expected = "[1,2]#0:fd\n[1,2]#13:(\n[1,3]#4:50\n[1,5]#14:)\n"
                    "[2,3]#0:fd\n[2,3]#13:(\n[2,4]#4:50\n[2,6]#14:)\n[2,7]#25:\n";
    auto code = "fd(50)\nfd(50)";

    Lexer lexer(code);
    auto tokens = lexer.lex();
    for (const auto &token:tokens) {
        token->print(oss);
    }

    TEST_ASSERT_EQUAL_MESSAGE(9, tokens.size(), code);
    TEST_ASSERT_EQUAL_STRING_MESSAGE(expected, oss.str().c_str(), code);
}

void test_lex_square() {
    auto code = "repeat 4 [fd(100) rt(90)]";

    Lexer lexer(code);
    auto tokens = lexer.lex();

    TEST_ASSERT_EQUAL_MESSAGE(13, tokens.size(), code);
}

void test_lex_multiline() {
    auto code = "repeat 4 \r\n[\n\tfd(100)\n\trt(90)\n]\r\n";

    Lexer lexer(code);
    auto tokens = lexer.lex();

    TEST_ASSERT_EQUAL_MESSAGE(13, tokens.size(), code);
}

void test_lex_alltokens() {
    auto code =
            "fd to end :count 50 \" if else repeat true false [ ] ( ) + - * / = != < > # ,";
    std::vector<int> tokenTypes{0, 1, 2, 3, 4, 23, 6, 7, 8, 9, 10, 11, 12, 13,
                                14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25};
    std::vector<const char *> tokenValues{"fd", "to", "end", "count", "50",
                                          "\"", "if", "else", "repeat", "true", "false", "[", "]", "(", ")",
                                          "+", "-", "*", "/", "=", "!=", "<", ">", "#", ",", nullptr};
    Lexer lexer(code);
    auto tokens = lexer.lex();

    TEST_ASSERT_EQUAL_MESSAGE(26, tokens.size(), code);

    auto typeIt = tokenTypes.begin();
    auto literalIt = tokenValues.begin();
    for (const auto &token:tokens) {
        TEST_ASSERT_EQUAL_MESSAGE(*typeIt, token->type, token->literal);
        TEST_ASSERT_EQUAL_STRING_MESSAGE(*literalIt, token->literal,
                                         *literalIt);
        ++typeIt;
        ++literalIt;
    }
}

void test_lex_function_newline() {
    auto code =
            "to function :param\nfd";
    std::vector<int> tokenTypes{1, 0, 3, 0, 25};
    std::vector<const char *> tokenValues{"to", "function", "param", "fd", nullptr};
    Lexer lexer(code);
    auto tokens = lexer.lex();

    TEST_ASSERT_EQUAL_MESSAGE(5, tokens.size(), code);

    auto typeIt = tokenTypes.begin();
    auto literalIt = tokenValues.begin();
    for (const auto &token:tokens) {
        TEST_ASSERT_EQUAL_MESSAGE(*typeIt, token->type, token->literal);
        TEST_ASSERT_EQUAL_STRING_MESSAGE(*literalIt, token->literal,
                                         *literalIt);
        ++typeIt;
        ++literalIt;
    }
}

void run_test_lex() {
    UNITY_BEGIN();

    RUN_TEST(test_lex_simple_fd);
    RUN_TEST(test_lex_multiline_fd);
    RUN_TEST(test_lex_square);
    RUN_TEST(test_lex_multiline);
    RUN_TEST(test_lex_alltokens);
    RUN_TEST(test_lex_function_newline);

    UNITY_END();
}
