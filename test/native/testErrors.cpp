#include <unity.h>
#include <stdio.h>
#include <sstream>
#include "LogoInterpreter.h"
#include "testdefs.h"

std::string tests[][3] = {
        {"fKKd(50)",                           "",                                        "[1,4]:Identifier 'fKKd' not found.\n"},
        {"fd(kk)",                             "",                                        "[1,5]:Identifier 'kk' not found.\n"},
        {"fd(:kk)",                            "",                                        "[1,4]:Variable kk not found\n"},
        {"repeat 4 [ fd(50) fKKd(50)]",        "Forward: 50\n",                           "[1,22]:Identifier 'fKKd' not found.\n"},
        {"repeat 4 [ fd(50)",                  "",                                        "[1,13]:Block statement not closed\n"},
        {"repeat 4 fd(50)",                    "",                                        "[1,11]:Unexpected token 'fd'\n"},
        {"repeat [ fd(50)]",                   "",                                        "[1,7]:Unexpected token '['\n[1,7]:no prefix parse function for [\n[1,15]:no prefix parse function for ]\n"},
        {"repeat a [ fd(50) fKKd(50)]",        "",                                        "[1,8]:Unexpected token 'a'\n[1,9]:no prefix parse function for [\n[1,26]:no prefix parse function for ]\n"},
        {"fd(50 / 0)",                         "",                                        "[1,6]:Division by 0\n"},
        {"fd(50 / 0, 20)",                     "",                                        "[1,6]:Division by 0\n"},
        {"if( 1 ) [ fd(50) ] else [ bk(50) ]", "",                                        "[1,2]:if expression must evaluate to boolean\n"},
        {"if( bk(50) ) [ fd(50) ]",            "Backward: 50\n",                          "[1,2]:if expression must evaluate to boolean\n"},
        {"to myfunc fd(50) myfunc()",          "",                                        "[1,2]:Missing end for procedure\n"},
        {"to myfunc :one end myfunc()",        "",                                        "[1,25]:Expecting 1 actual parameter(s)\n"},
        {"to myfunc :one end myfunc(1,2)",     "",                                        "[1,25]:Expecting 1 actual parameter(s)\n"},
        {"to myfunc :one fd(50) :one() end "
         "myfunc(1)",                          "Forward: 50\n",                           "[1,26]:Invalid function value\n"},
        {"to rekurziv :param\n"
         "fd(:param)\n"
         "if(:param > 10) [rekurziv(:param-10)]\n"
         "bKKk(:param)\n"
         "end\n"
         "rekurziv(30)",                       "Forward: 30\nForward: 20\nForward: 10\n", "[4,5]:Identifier 'bKKk' not found.\n"},

};
int rows = sizeof tests / sizeof tests[0];
int test_index = 0;

std::ostringstream error_oss;

LogoRuntimeValue *test_error_logo_fd(std::vector<LogoRuntimeValue *> &params) {
    auto param1 = params[0]->getDouble();
    error_oss << "Forward: " << param1 << std::endl;
    return new IntValue(216);
}

LogoRuntimeValue *test_error_logo_bk(std::vector<LogoRuntimeValue *> &params) {
    auto param1 = params[0]->getDouble();
    error_oss << "Backward: " << param1 << std::endl;
    return new IntValue(215);
}

void run_error_test() {
    error_oss.str("");
    error_oss.clear();

    const char *code = tests[test_index][0].c_str();
    const char *expectedExec = tests[test_index][1].c_str();
    const char *expectedError = tests[test_index][2].c_str();
    auto error1 = tests[test_index][0] + " Should fail";
    auto error2 = tests[test_index][0] + " Should produce this error";
    auto error3 = tests[test_index][0] + " Should run";
    std::string error = "";
    bool hasError = false;
    std::cout << code << std::endl;

    {
        LogoInterpreter interpreter;
        interpreter.registerBuiltInFunc("fd", &test_error_logo_fd);
        interpreter.registerBuiltInFunc("bk", &test_error_logo_bk);
        interpreter.execute(code);
        hasError = interpreter.hasErrors();
        if (hasError) {
            error = interpreter.asOneError();
        }
    }


    TEST_ASSERT_TRUE_MESSAGE(hasError, error1.c_str());
    TEST_ASSERT_EQUAL_STRING_MESSAGE(expectedError, error.c_str(), error2.c_str());
    TEST_ASSERT_EQUAL_STRING_MESSAGE(expectedExec, error_oss.str().c_str(), error3.c_str());
    error_oss.str("");
    error_oss.clear();
}

void run_test_errors() {
    UNITY_BEGIN();
    for (test_index = 0; test_index < rows; ++test_index) {
        RUN_TEST(run_error_test);
    }
    UNITY_END();
}
