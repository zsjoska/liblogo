#include <unity.h>
#include <stdio.h>
#include <sstream>
#include "LogoInterpreter.h"
#include "testdefs.h"


std::ostringstream oss;

LogoRuntimeValue *test_interpret_logo_fd(std::vector<LogoRuntimeValue *> &params) {
    auto param1 = params[0]->getDouble();
    oss << "Forward: " << param1 << std::endl;
    return new IntValue(216);
}

LogoRuntimeValue *test_interpret_logo_bk(std::vector<LogoRuntimeValue *> &params) {
    auto param1 = params[0]->getDouble();
    oss << "Backward: " << param1 << std::endl;
    return new IntValue(215);
}

LogoRuntimeValue *test_interpret_logo_rt(std::vector<LogoRuntimeValue *> &params) {
    auto param1 = params[0]->getDouble();
    oss << "Right: " << param1 << std::endl;
    return new IntValue(215);
}

LogoRuntimeValue *test_interpret_logo_lt(std::vector<LogoRuntimeValue *> &params) {
    auto param1 = params[0]->getDouble();
    oss << "Left: " << param1 << std::endl;
    return new IntValue(215);
}

LogoRuntimeValue *test_interpret_logo_bool(std::vector<LogoRuntimeValue *> &params) {
    auto param1 = params[0]->getBool();
    oss << "bool: " << param1 << std::endl;
    return new IntValue(215);
}

void test_interpret_simple_fd() {
    oss.str("");
    oss.clear();
    auto expected = "Forward: 50\nBackward: 20\n";
    auto code = "fd(50) bk(20)";

    LogoInterpreter interpreter;
    interpreter.registerBuiltInFunc("fd", &test_interpret_logo_fd);
    interpreter.registerBuiltInFunc("bk", &test_interpret_logo_bk);
    interpreter.execute(code);

    TEST_ASSERT_EQUAL_STRING_MESSAGE(expected, oss.str().c_str(), code);
    oss.str("");
    oss.clear();
}

void test_interpret_multi_call() {
    oss.str("");
    oss.clear();
    auto expected = "Forward: 50\nBackward: 20\nForward: 50\nBackward: 20\n";
    auto code = "fd(50) bk(20)";

    LogoInterpreter interpreter;
    interpreter.registerBuiltInFunc("fd", &test_interpret_logo_fd);
    interpreter.registerBuiltInFunc("bk", &test_interpret_logo_bk);
    auto program = interpreter.parse(code);
    auto runtime = interpreter.getRuntime();
    delete runtime->run(program);
    delete runtime->run(program);
    delete program;
    TEST_ASSERT_EQUAL_STRING_MESSAGE(expected, oss.str().c_str(), code);
    oss.str("");
    oss.clear();
}

void test_interpret_runtime_keep() {
    oss.str("");
    oss.clear();
    auto expected = "Forward: 50\n";
    auto code1 = "to elore :param\n"
                "fd(:param)\n"
                "end\n";
    auto code2 = "elore(50)";

    LogoInterpreter interpreter;
    interpreter.registerBuiltInFunc("fd", &test_interpret_logo_fd);
    interpreter.registerBuiltInFunc("bk", &test_interpret_logo_bk);
    auto runtime = interpreter.getRuntime();
    auto program = interpreter.parse(code1);
    delete runtime->run(program);
    delete program;

    program = interpreter.parse(code2);
    delete runtime->run(program);
    delete program;
    TEST_ASSERT_EQUAL_STRING_MESSAGE(expected, oss.str().c_str(), code2);
    oss.str("");
    oss.clear();
}


void test_interpret_simple_bool() {
    oss.str("");
    oss.clear();
    auto expected = "bool: 1\n";
    auto code = "bool(true)";

    LogoInterpreter interpreter;
    interpreter.registerBuiltInFunc("fd", &test_interpret_logo_fd);
    interpreter.registerBuiltInFunc("bk", &test_interpret_logo_bk);
    interpreter.registerBuiltInFunc("bool", &test_interpret_logo_bool);
    interpreter.execute(code);

    TEST_ASSERT_EQUAL_STRING_MESSAGE(expected, oss.str().c_str(), code);
    oss.str("");
    oss.clear();
}

void test_interpret_simple_square() {
    oss.str("");
    oss.clear();
    auto expected = "Forward: 100\nRight: 90\nForward: 100\nRight: 90\nForward: 100\nRight: 90\nForward: 100\nRight: 90\n";
    auto code = "repeat 4 [fd(100) rt(90)]";

    LogoInterpreter interpreter;
    interpreter.registerBuiltInFunc("fd", &test_interpret_logo_fd);
    interpreter.registerBuiltInFunc("bk", &test_interpret_logo_bk);
    interpreter.registerBuiltInFunc("rt", &test_interpret_logo_rt);
    interpreter.execute(code);

    TEST_ASSERT_EQUAL_STRING_MESSAGE(expected, oss.str().c_str(), code);
    oss.str("");
    oss.clear();
}

void test_interpret_tree() {
    oss.str("");
    oss.clear();
    auto expected = 121980;
    auto code = "to tree :size\n"
                "  if (:size < 5) [\n"
                "    fd(:size) bk(:size)\n"
                "  ] else [\n"
                "    fd(:size/3)  \n"
                "    lt(30) tree(:size*2/3) rt(30)\n"
                "    fd(:size/6)\n"
                "    rt(25) tree(:size/2) lt(25)\n"
                "    fd(:size/3)\n"
                "    rt(25) tree(:size/2) lt(25)\n"
                "    fd(:size/6)\n"
                "    bk(:size)\n"
                "  ]\n"
                "end\n"
                "\n"
                "tree(150)";

    LogoInterpreter interpreter;
    interpreter.registerBuiltInFunc("fd", &test_interpret_logo_fd);
    interpreter.registerBuiltInFunc("bk", &test_interpret_logo_bk);
    interpreter.registerBuiltInFunc("rt", &test_interpret_logo_rt);
    interpreter.registerBuiltInFunc("lt", &test_interpret_logo_lt);
    interpreter.execute(code);

    auto str = oss.str();
//    std::cout << str << std::endl;
    TEST_ASSERT_EQUAL_MESSAGE(expected, str.size(), code);
    oss.str("");
    oss.clear();
}


void test_interpret_parser_error() {
    oss.str("");
    oss.clear();
    auto expected = "";
    auto code = "fd(50bk)";

    LogoInterpreter interpreter;
    interpreter.registerBuiltInFunc("fd", &test_interpret_logo_fd);
    interpreter.registerBuiltInFunc("bk", &test_interpret_logo_bk);
    interpreter.execute(code);

    TEST_ASSERT_TRUE_MESSAGE(interpreter.hasErrors(), code);
    TEST_ASSERT_EQUAL_STRING_MESSAGE(expected, oss.str().c_str(), code);
    oss.str("");
    oss.clear();
}

void test_interpret_missing_func_error() {
    oss.str("");
    oss.clear();
    auto expectedExec = "";
    auto expectedError = "[1,4]:Identifier 'fKKd' not found.\n";
    auto code = "fKKd(50)";

    LogoInterpreter interpreter;
    interpreter.registerBuiltInFunc("fd", &test_interpret_logo_fd);
    interpreter.registerBuiltInFunc("bk", &test_interpret_logo_bk);
    interpreter.execute(code);

    TEST_ASSERT_TRUE_MESSAGE(interpreter.hasErrors(), code);
    TEST_ASSERT_EQUAL_STRING_MESSAGE(expectedError, interpreter.asOneError().c_str(), code);
    TEST_ASSERT_EQUAL_STRING_MESSAGE(expectedExec, oss.str().c_str(), code);
    oss.str("");
    oss.clear();
}

void run_test_interpret() {
    UNITY_BEGIN();

    RUN_TEST(test_interpret_simple_fd);
    RUN_TEST(test_interpret_multi_call);
    RUN_TEST(test_interpret_simple_bool);
    RUN_TEST(test_interpret_simple_square);
    RUN_TEST(test_interpret_tree);
    RUN_TEST(test_interpret_parser_error);
    RUN_TEST(test_interpret_missing_func_error);
    RUN_TEST(test_interpret_runtime_keep);

    UNITY_END();
}
