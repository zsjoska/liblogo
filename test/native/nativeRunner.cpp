#ifdef UNIT_TEST

#include <unity.h>

#include "testdefs.h"

int main( int argc, char **argv) {

    run_test_lex();
    run_test_parse();
    run_test_interpret();
    run_test_bulks();
    run_test_errors();
}

#endif
